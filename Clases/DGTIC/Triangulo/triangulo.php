<html>
<head>
	<title>Piramide</title>
</head>
<body>
	<article>
		<?php
			function piramide($limite) {
   				for($fila = 1; $fila < $limite; $fila ++) {
        			$asteriscos = str_repeat('*', ($fila - 1) * 2 + 1);
        			$espacio = str_repeat(' ', $limite - $fila);
        			echo $espacio . $asteriscos . '<br/>';
    			}
			}
			echo "<pre>" ;
			piramide(10);
		?>
	</article>
</body>
</html>