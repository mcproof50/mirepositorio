<!DOCTYPE html>
<html>
<head>
	<title>Investigación</title>
</head>
<body>
<?php
	/* 1.	Substr: Devuelve parte de una cadena
Ejemplo Usando un start negativo
<?php
$rest = substr("abcdef", -1);    // devuelve "f"
$rest = substr("abcdef", -2);    // devuelve "ef"
$rest = substr("abcdef", -3, 1); // devuelve "d"
?> 
2.	Strstr: Encuentra la primera aparición de un string
Ejemplo de strstr()
<?php
$email  = 'name@example.com';
$domain = strstr($email, '@');
echo $domain; // mostrará @example.com

$user = strstr($email, '@', true); // Desde PHP 5.3.0
echo $user; // mostrará name
?> 
3.	Strpos: Encuentra la posición de la primera ocurrencia de un substring en un string
Ejemplo Usando ===
<?php
$mystring = 'abc';
$findme   = 'a';
$pos = strpos($mystring, $findme);

// Nótese el uso de ===. Puesto que == simple no funcionará como se espera
// porque la posición de 'a' está en el 1° (primer) caracter.
if ($pos === false) {
    echo "La cadena '$findme' no fue encontrada en la cadena '$mystring'";
} else {
    echo "La cadena '$findme' fue encontrada en la cadena '$mystring'";
    echo " y existe en la posición $pos";
}
?> 
4.	Implode: Une elementos de un array en un string
Ejemplo de implode()
<?php

$array = array('apellido', 'email', 'teléfono');
$separado_por_comas = implode(",", $array);

echo $separado_por_comas; // apellido,email,teléfono

// Devuelve un string vacío si se usa un array vacío:
var_dump(implode('hola', array())); // string(0) ""

?> 
5.	Explode: Divide un string en varios string
Ejemplo de explode()
<?php
// Ejemplo 1
$pizza  = "porción1 porción2 porción3 porción4 porción5 porción6";
$porciones = explode(" ", $pizza);
echo $porciones[0]; // porción1
echo $porciones[1]; // porción2

// Ejemplo 2
$datos = "foo:*:1023:1000::/home/foo:/bin/sh";
list($user, $pass, $uid, $gid, $gecos, $home, $shell) = explode(":", $datos);
echo $user; // foo
echo $pass; // *

?> 
6.	utf8_encode: Codifica un string ISO-8859-1 a UTF-8
Ejemplo
function isUTF8($string)
{
    if (is_array($string))
    {
        $enc = implode('', $string);
        return @!((ord($enc[0]) != 239) && (ord($enc[1]) != 187) && (ord($enc[2]) != 191));
    }
    else
    {
        return (utf8_encode(utf8_decode($string)) == $string);
    }    
}
?>
7.	utf8_decode: Convierte una cadena con los caracteres codificados ISO-8859-1 con UTF-8 a un sencillo byte ISO-8859-1
Ejemplo  
$text=utf8_decode($text);
8.	array_pop: extrae el último elemento del final del array
Ejemplo de array_pop()
<?php
$stack = array("naranja", "plátano", "manzana", "frambuesa");
$fruit = array_pop($stack);
print_r($stack);
?> 
9.	array_push: Inserta uno o más elementos al final de un array
Ejemplo de array_push()
<?php
$pila = array("naranja", "plátano");
array_push($pila, "manzana", "arándano");
print_r($pila);
?> 
10.	array_diff: Calcula la diferencia entre arrays. Compara array1 con uno o más arrays y devuelve los valores de array1 que no estén presentes en ninguno de los otros arrays.
Ejemplo de array_diff()
<?php
$array1    = array("a" => "green", "red", "blue", "red");
$array2    = array("b" => "green", "yellow", "red");
$resultado = array_diff($array1, $array2);

print_r($resultado);
?> 
11.	array_walk: Aplicar una función proporcionada por el usuario a cada miembro de un array. Aplica la función definida por el usuario dada por callback a cada elemento del array dado por array. array_walk() no le afecta el puntero de arrays interno de array. array_walk() recorrerá el array completo sin tener en cuenta la posición del puntero.
Ejemplo de array_walk()
<?php
$frutas = array("d" => "limón", "a" => "naranja", "b" => "banana", "c" => "manzana");

function test_alter(&$elemento1, $clave, $prefijo)
{
    $elemento1 = "$prefijo: $elemento1";
}

function test_print($elemento2, $clave)
{
    echo "$clave. $elemento2<br />\n";
}

echo "Antes ...:\n";
array_walk($frutas, 'test_print');

array_walk($frutas, 'test_alter', 'fruta');
echo "... y después:\n";

array_walk($frutas, 'test_print');
?> 
12.	sort: Ordena un array. Esta función ordena un array. Los elementos estarán ordenados de menor a mayor cuando la función haya terminado.
Ejemplo de sort()
<?php

$frutas = array("limón", "naranja", "banana", "albaricoque");
sort($frutas);
foreach ($frutas as $clave => $valor) {
    echo "frutas[" . $clave . "] = " . $valor . "\n";
}

?> 
13.	current: Devuelve el elemento actual en un array. Cada array tiene un puntero interno a su elemento "actual", que es iniciado desde el primer elemento insertado en el array.
Ejemplo de uso de current() y similares
<?php
$transport = array('pie', 'bici', 'coche', 'avión');
$mode = current($transport); // $mode = 'pie';
$mode = next($transport);    // $mode = 'bici';
$mode = current($transport); // $mode = 'bici';
$mode = prev($transport);    // $mode = 'pie';
$mode = end($transport);     // $mode = 'avión';
$mode = current($transport); // $mode = 'avión';

$arr = array();
var_dump(current($arr)); // bool(false)

$arr = array(array());
var_dump(current($arr)); // array(0) { }
?> 
14.	date: Dar formato a la fecha/hora local. Devuelve una cadena formateada según el formato dado usando el parámetro de tipo integer timestamp dado o el momento actual si no se da una marca de tiempo. En otras palabras, timestamp es opcional y por defecto es el valor de time().
Ejemplo de date()
<?php
// Establecer la zona horaria predeterminada a usar. Disponible desde PHP 5.1
date_default_timezone_set('UTC');


// Imprime algo como: Monday
echo date("l");

// Imprime algo como: Monday 8th of August 2005 03:12:46 PM
echo date('l jS \of F Y h:i:s A');

// Imprime: July 1, 2000 is on a Saturday
echo "July 1, 2000 is on a " . date("l", mktime(0, 0, 0, 7, 1, 2000));

/* Usar las constantes en el parámetro de formato */
// Imprime algo como: Wed, 25 Sep 2013 15:28:57 -0700
/*echo date(DATE_RFC2822);

// Imprime algo como: 2000-07-01T00:00:00+00:00
echo date(DATE_ATOM, mktime(0, 0, 0, 7, 1, 2000));
?> 
15.	empty: Determina si una variable está vacía. Determina si una variable es considerada vacía. Una variable se considera vacía si no existe o si su valor es igual a FALSE. empty() no genera una advertencia si la variable no existe.
Ejemplo Una simple comparación empty() / isset() 
<?php
$var = 0;

// Se evalúa a true ya que $var está vacia
if (empty($var)) {
    echo '$var es o bien 0, vacía, o no se encuentra definida en absoluto';
}

// Se evalúa como true ya que $var está definida
if (isset($var)) {
    echo '$var está definida a pesar que está vacía';
}
?> 


16.	isset: Determina si una variable está definida y no es NULL. Si una variable ha sido removida con unset(), esta ya no estará definida. isset() devolverá FALSE si prueba una variable que ha sido definida como NULL. También tenga en cuenta que un byte NULL ("\0") no es equivalente a la constante NULL de PHP. Si son pasados varios parámetros, entonces isset() devolverá TRUE únicamente si todos los parámetros están definidos. La evaluación se realiza de izquierda a derecha y se detiene tan pronto como se encuentre una variable no definida.
Ejemplos isset()
<?php

$var = '';

// Esto evaluará a TRUE así que el texto se imprimirá.
if (isset($var)) {
    echo "Esta variable está definida, así que se imprimirá";
}

// En los siguientes ejemplo usaremos var_dump para imprimir
// el valor devuelto por isset().

$a = "prueba";
$b = "otraprueba";

var_dump(isset($a));      // TRUE
var_dump(isset($a, $b)); // TRUE

unset ($a);

var_dump(isset($a));     // FALSE
var_dump(isset($a, $b)); // FALSE

$foo = NULL;
var_dump(isset($foo));   // FALSE

?> 
17.	serialize: Genera una representación apta para el almacenamiento de un valor. Esto es útil para el almacenamiento de valores en PHP sin perder su tipo y estructura. Para recuperar el valor PHP a partir de la cadena seriada, utilice unserialize().
Ejemplo serialize()
<?php
// $datos_sesion contiene un array multi-dimensional con
// información del usuario actual. Usamos serialize() para
// almacenarla en una base de datos al final de la petición.

$con  = odbc_connect("bd_web", "php", "gallina");
$sent = odbc_prepare($con,
      "UPDATE sesiones SET datos = ? WHERE id = ?");
$datos_sql = array (serialize($datos_sesion), $_SERVER['PHP_AUTH_USER']);

if (!odbc_execute($sent, &$datos_sql)) {
    $sent = odbc_prepare($con,
     "INSERT INTO sesiones (id, datos) VALUES(?, ?)");
    if (!odbc_execute($sent, &$datos_sql)) {
        /* Algo ha fallado.. */
        /*
    }
}
?> 

18.	unserialize: crea un valor PHP a partir de una representación almacenada. Toma una única variable serializada y la vuelve a convertir a un valor de PHP.
 Ejemplo de unserialize()
<?php
// Aquí usamos unserialize() para cargar los datos de sesión
// provenientes del string seleccionado desde la base de datos en el
// array $datos_sesion. Este ejemplo complementa aquel descrito con serialize().

$con  = odbc_connect("bd_web", "php", "gallina");
$sent = odbc_prepare($con, "SELECT datos FROM sesiones WHERE id = ?");
$datos_sql = array($_SERVER['PHP_AUTH_USER']);

if (!odbc_execute($sent, &$datos_sql) || !odbc_fetch_into($sent, &$tmp)) {
    // si la ejecución del comando o la recuperación de datos falla,
    // inicializar una matriz vacía
    $datos_sesion = array();
} else {
    // ahora deberíamos tener los datos serializados en $tmp[0].
    $datos_sesion = unserialize($tmp[0]);
    if (!is_array($datos_sesion)) {
        // algo ha fallado, inicializar un array vacío
        $datos_sesion = array();
    }
}
?> 



?> */
</body>
</html>